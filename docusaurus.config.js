// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require("prism-react-renderer/themes/github");
const darkCodeTheme = require("prism-react-renderer/themes/dracula");

/** @type {import('@docusaurus/types').Config} */
const config = {
    title: "Paylican API Documentation | Paylican LLC",
    tagline: "Paylican Bill and Payment API Documentation",
    url: "https://docs.paylican.com",
    baseUrl: "/",
    onBrokenLinks: "throw",
    onBrokenMarkdownLinks: "warn",
    favicon: "img/favicon.ico",
    organizationName: "paylican",
    projectName: "payment-platform",

    presets: [
        [
            "docusaurus-preset-openapi",
            /** @type {import('docusaurus-preset-openapi').Options} */
            ({
                api: {
                    id: 'bill',
                    path: "openapi/iss.json",
                    routeBasePath: "/bill-api",
                },
                docs: {
                    sidebarPath: require.resolve("./sidebars.js"),
                    editUrl:
                        "https://gitlab.com/dmitrii.erokhin/paylican-docs/-/blob/master/",
                },
                blog: {
                    showReadingTime: true,
                    editUrl:
                        "https://gitlab.com/dmitrii.erokhin/paylican-docs/-/blob/master/",
                },
                theme: {
                    customCss: require.resolve("./src/css/custom.css"),
                },
            }),
        ],
    ],

    plugins: [
        [
            'docusaurus-plugin-openapi',
            {
                id: 'pgw',
                path: 'openapi/pgw.json',
                routeBasePath: '/payment-api',
            },
        ]
    ],

    themeConfig:
    /** @type {import('docusaurus-preset-openapi').ThemeConfig} */
        ({
            navbar: {
                title: ".com",
                logo: {
                    alt: "Paylican Payment Platform logo",
                    src: "img/logo.svg",
                },
                items: [
                    {
                        type: "doc",
                        docId: "intro",
                        position: "left",
                        label: "Documentation",
                    },
                    {to: "/bill-api", label: "Bill API", position: "left"},
                    {to: "/payment-api", label: "Payment API", position: "left"},
                ],
            },
            prism: {
                theme: lightCodeTheme,
                darkTheme: darkCodeTheme,
            },
            algolia: {
                appId: 'H8GGTIVJM9',
                apiKey: 'd737844cf6a664c3525c33dc4c240267',
                indexName: 'docs_idx',
                contextualSearch: true,
                searchParameters: {},
                // Optional: path for search page that enabled by default (`false` to disable it)
                searchPagePath: 'search',
            },
        }),
};

module.exports = config;
