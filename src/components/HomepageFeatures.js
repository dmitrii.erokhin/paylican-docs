import React from "react";
import clsx from "clsx";
import styles from "./HomepageFeatures.module.css";

const FeatureList = [
    {
        title: "Bill API",
        description: (
            <>
                The Bill API provide endpoints for Merchant functionality.
            </>
        ),
        link: "/bill-api"
    },
    {
        title: "Getting Started",
        description: (
            <>
                Paylican is a platform for online payments via a direct bank transfer.
            </>
        ),
        link: "/docs/intro"
    },
    {
        title: "Payment Platform API",
        description: (
            <>
                The payment platform offers Economic Operators (OEs) the possibility to pay their invoices
                electronically.
            </>
        ),
        link: "/docs/payment-platform-api/overview"
    },
    {
        title: "Bank API",
        description: (
            <>
                The Bank API provide endpoints for the Direct Payment Flow: Authentication and Execute Payment.
            </>
        ),
        link: "/docs/bank-api/overview"
    },
];

function Feature({Src, title, description, link}) {
    return (
        <div className={clsx("col col--3", styles.featuresCol)}>
            {Src && <div className="text--center">
                <img src={Src} className={styles.featureSvg} alt={title}/>
            </div>}
            <div className="text--center padding-horiz--md">
                <h3>{title}</h3>
                <p>{description}</p>
                <a className="button button--primary button--md" href={link}>More...</a>
            </div>
        </div>
    );
}

export default function HomepageFeatures() {
    return (
        <section className={styles.features}>
            <div className="container">
                <div className="row">
                    {FeatureList.map((props, idx) => (
                        <Feature key={idx} {...props} />
                    ))}
                </div>
            </div>
        </section>
    );
}
