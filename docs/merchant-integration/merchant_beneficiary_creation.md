---
sidebar_position: 1
---

# Merchant and beneficiaries creation

This section encompasses essential details required to set up and establish a merchant profile within the Paylican Invoice System. This data serves as a comprehensive guide, outlining the specific parameters and particulars necessary to facilitate a seamless and accurate creation process for merchants utilizing the system. 

|Object|Attribute|Mandatory/Optional|
|-|-|-|
|Merchant information|code|mandatory|
|     |name             |mandatory|
|     |tin              |optional|
|     |address	        |optional|
|     |invoice type	    |mandatory|
|     |show items for the invoice 	|true/false (optional, default will be true)|
|     |webhook URL	    |mandatory|
|     |merchant logo in PNG and SVG format	|optional|
|     |super merchant code	|optional|
|Merchant Address Information|	street	|optional|
|     |	address number	|optional|
|     |	phone	          |optional|
|     |	zip code	      |optional|
|     |	city	          |optional|
|     |	country	        |optional|
|Beneficiary Information	|code| 	mandatory| 
|     |	name            |	mandatory|
|     |	merchant code	  |mandatory|
|Beneficiary Account Information	|beneficiary code 	|mandatory|
|     |	account number	|mandatory|
|     |	account description	|mandatory|
|     |	bic	|mandatory (can be optional as we have examples in the database already so we can identify from account number)|
|     |	beneficiary bank code 	|optional (it's the first 5 digits of the account number)|
|     |	collector code 	|optional (it's the collector_code from beneficiary_bank table and the value of that is the field mentioned above)|
