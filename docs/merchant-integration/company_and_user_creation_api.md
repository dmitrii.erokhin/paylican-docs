---
sidebar_position: 2
---

# Company and User Creation API

This section describes the Company and User Creation API which allows merchant system to pass the required information to Paylican as soon as a company and a user are validated in merchant system.


## Methods and Endpoints

### Get Access Token
- HTTP Method: POST 
- URL: {{Keycloak URL}}
- Header: 'Content-Type: application/x-www-form-urlencoded' 
- Request Body:  
``` 
'username=*****' 
'password=*********' 
'grant_type=password' 
'client_id=invoice-system-client'
```

### Create Company
- HTTP Method: PUT
-	URL and Endpoint: {{User Management URL}}/api/v1/customer/{customerCode}
-	Request Body *please see sample codes below

#### Request body
```
{
  "name": "COMPANY SARL",
  "tin": "331234",
  "address": {
    "street": "",
    "addressNumber": "",
    "phone": "",
    "zipCode": "",
    "city": "",
    "country": ""
  }
}
```

### Create User
- HTTP Method: POST
-	URL and Endpoint: {{User Management URL}}/api/v1/user/{username}/customers/{customerCode}
-	Request Body *please see sample codes below

#### Request body
```
{
  "firstName": "John",
  "lastName": "Smith",
  "email": "john.smith@company.com",
  "userGroupUuid": "ee31cabc-571f-4a88-be00-d7d159237a68",
  "address": {
    "street": "",
    "phone": "",
    "zipCode": "",
    "city": "",
    "country": ""
  }
}
```
### Deactivate User
- HTTP Method: DELETE
-	URL and Endpoint: {{User Management URL}}/api/v1/user/{username}

