---
sidebar_position: 3
---

# Invoice Creation API

This section serves as a robust tool by furnishing an extensive array of essential data necessary for the seamless generation of invoices within the Paylican Invoice System. 

This API encompasses a detailed list of information, including but not limited to client details, transaction specifics, and itemized services or products. Its purpose is to facilitate a streamlined and efficient process, ensuring that invoices are accurately crafted and aligned with the intricacies of the Paylican Invoice System.

## Methods and Endpoints

### Get Access Token
- HTTP Method: POST 
- URL: {{Keycloak URL}}
- Header: 'Content-Type: application/x-www-form-urlencoded' 
- Request Body:  
``` 
'username=*****' 
'password=*********' 
'grant_type=password' 
'client_id=invoice-system-client'
```

### Create Invoice
- HTTP Method: POST
-	URL and Endpoint: {{Invoice System URL}}
-	Request Body *please see sample codes below

### Void Invoice
- HTTP Method: PATCH
- URL and Endpoint: {{Invoice System URL}}/{{invoice_id}}/void
- Request Body: N/A

## Mandatory fields for API
Invoice creation is executed by calling Create Invoice Endpoint from the external merchant system.  All the fields are filled in our Invoice system from external merchant systems' call. 

The Create Invoice Endpoint is called by the merchant and if the mandatory parameters are filled in, the system creates the invoice with the appropriate fields in the Paylican Invoice System. 

Below are the fields that are needed to be filled in for creating the Invoice in the "Paylican Invoice" application.

- HTTP Method: POST
- Header
  - Authorization: Bearer TOKEN
  - Accepted Format: JSON

Below are the details of the Invoice object, which serves as the input payload for this web service:
- Invoice object
- Item object
- Tax object
- Beneficiary object
- Additional Field object
- Response Attributes

### Invoice object
| Parameter Name                                         | Data Type                    | Mandatory/ Optional                              | Description                                                                                                                 |
|--------------------------------------------------------|------------------------------|--------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------|
| id (only used in responses)                            | String                       | mandatory                                        | Invoice Reference Number                                                                                                    |
| type	                                                  | String	                      | mandatory	                                       | Invoice Type                                                                                                                |
| date	                                                  | String	                      | mandatory	                                       | Invoice creation date and time.                                                                                             |
| amount	                                                | bigDecimal	                  | mandatory	                                       | Amount to be paid. The amounts are represented by String with two decimal places and a point separating these two decimals. |
| currency	                                              | String                       | 	mandatory	                                      | Currency code                                                                                                               |
| totalAmount	                                           | bigDecimal	                  | mandatory	                                       | Total amount to be paid for the invoice.                                                                                    |
| customerCode, customerEmail, customerTaxIdentification | 	String                      | 	at least one of these 3 parameters is mandatory | 	The customer's code that is billed and has to pay the invoice.                                                             |
| customerName (only used in responses)                  | 	String                      | 	mandatory                                       | 	The customer's name that has to pay the invoice.                                                                           |
| customerAddress (only used in responses)               | 	String                      | 	mandatory                                       | 	The customer's address that has to pay the invoice.                                                                        |
| voidedAt (only in responses)                           | 	String                      | 	mandatory                                       | 	Time at which the invoice was voided.                                                                                      |
| merchantCode                                           | 	String                      | 	mandatory                                        | 	Merchant Code of the invoice issuer                                                                                        |
| merchantReference                                      | 	String	                     | mandatory	                                        | Merchant internal reference of the document associated to the Invoice.                                                      |
| expirationDate                                         | 	String	                     | optional	                                        | Date and time of expiration of the invoice.                                                                                 |
| merchantReferenceURL                                   | 	String                      | 	optional                                        | 	URL to the merchant system that display the internal reference document                                                    |
| representativeCode	                                    | String	                      | optional                                         | 	It is the authorized representative or agent, i.e. the customer code of another company which can pay the invoice.         |
| representativeName (only used in responses)            | 	String                      | 	optional	                                       | Name of the Authorized Representative that can pay the invoice.                                                             |
| representativeAddress (only used in responses)         | 	String	                     | optional                                         | 	Address of the Authorized Representative that can to pay the invoice.                                                      |
| tax                                                    | 	bigDecimal                  | 	optional	                                       | The amount of tax on this invoice.                                                                                          |
| discount	                                              | String	                      | optional                                         | 	Optional discount applied to the invoice.                                                                                  |
| invoiceURL (only used in responses)	                   | String	                      | optional	                                        | URL to be used to link to the resource showing the invoice                                                                  |
| userReference                                          | 	String	                     | optional	                                        | Reference used by third party (when applicable)                                                                             |
| receiptNumber                                          | 	String                      | 	optional	                                       | Receipt number in the third party system. Used for integration with external systems.                                       |
| items                                                  | 	```List<Item>```	           | optional	                                        | List of item lines for the invoice (refer to the Item object)                                                               |
| invoiceBeneficiaries                                   | 	```List<Beneficiary>```     | 	optional                                        | 	List of beneficiaries of the invoice (refer to the Beneficiary object)                                                     |
| additionalFields	                                      | ```List<AdditionalField>```	 | optional	                                        | Additional fields (up to 15) that can be used to customize the invoice (Refer to the Additonal Field object)                |
| createdAt (only in database)                           | String	                      | 	                                                | Time at which the invoice was created.                                                                                      |
| modifiedAt (only in database)                          | 	String	                     |                                                  | 	Time at which the invoice was modified.                                                                                    |
| Status (only in response)                              | String	                      | 	                                                | Invoice status                                                                                                              |

### Item Object
| Parameter Name     | Data Type       | Mandatory/ Optional | Description                                                                     |
|--------------------|-----------------|---------------------|---------------------------------------------------------------------------------|
| name	              | String          | 	mandatory	         | Item name identification                                                        |
| amountWithTaxes    | 	bigDecimal	    | mandatory	          | Amount All Taxes Included (in the currency specified) of the invoice            |
| code	              | String	         | optional            | 	Item code                                                                      |
| rank	              | String	         | optional            | 	Sorting rank for this item                                                     |
| date	              | String	         | optional	           | Item date of creation (if applicable)                                           |
| quantity           | 	String	        | optional            | 	Quantity of the item                                                           |
| basePrice          | 	String	        | optional            | 	Base Price (Unit Price) of the item (in the currency specified) of the invoice |
| amountWithoutTaxes | 	bigDecimal	    | optional            | 	Amount Excluding Taxes (in the currency specified) of the invoice              |
| beneficiaryCode	   | String	         | optional	           | Beneficiary code                                                                |
| taxes	             | ```List<Tax>``` | 	optional	          | List of Taxes (refer to Tax object)                                             |

### Tax Object
| Parameter Name | Data Type | Mandatory/ Optional | Description                                                 |
|----------------|-----------|---------------------|-------------------------------------------------------------|
| code	          | String	   | mandatory           | 	Tax code                                                   |
| rate	          | String	   | mandatory	          | Tax rate                                                    |
| amount         | 	String   | 	mandatory          | 	Tax Amount (in the currency specified) of the invoice item |
| description    | 	String	  | optional            | 	Description of the tax                                     |
| rank	          | String	   | optional	           | Sorting rank for this tax                                   |

### Beneficiary Object
| Parameter Name | Data Type | Mandatory/ Optional | Description                                                         |
|----------------|-----------|---------------------|---------------------------------------------------------------------|
| code	          | String	   | mandatory           | 	Beneficiary code                                                   |
| Amount	        | String	   | mandatory	          | Beneficiary Amount (in the currency specified) of the invoice item. |
| description	   | String	   | optional            | 	Description of the Beneficiary                                     |
| rank	          | String	   | optional	           | Sorting rank for this Beneficiary                                   |

### Additional Field Object
| Parameter Name | Data Type | Mandatory/ Optional | Description            |
|----------------|-----------|---------------------|------------------------|
| code	          | String	   | mandatory	          | Additional Field code  |
| value	         | String	   | mandatory	          | Additional Field value |

### Response Attributes
| Parameter Name | Data Type | Mandatory/ Optional | Description               |
|----------------|-----------|---------------------|---------------------------|
| invoiceNumber	 | string    | 	mandatory	         | ID of the created invoice |
| status         | 	string   | 	mandatory	         | Status of the invoice     |
| invoiceURL     | 	string	  | mandatory           | 	URL to the invoice       |

## Response Examples

### Create Invoice Request Example
```
{
  "type": "SAD",
  "date": "2021-10-16T11:43:00",
  "amount": 4297911,
  "currency": "XOF",
  "totalAmount": 4297911,
  "customerCode": "3202113292581",
  "representativeCode": "201304",
  "merchantReference": "2021-BJC02-C-7996",
  "merchantCode": "GUCE-CUSTOMS",
  "merchantReferenceURL": "https://guce.gouv.bj/sad/sad/open?documentNo=2021-BJC02-C-7996",
  "items": [
    {
      "code": "RI",
      "name": "REDEVANCE INFORMATIQUE",
      "amountWithTaxes": 10000,
      "beneficiaryCode": "OGA-TRESOR"
    },
    {
      "code": "ECT",
      "name": "ECOTAXE",
      "amountWithTaxes": 57283,
      "beneficiaryCode": "OGA-TRESOR"
    },
    {
      "code": "TEL",
      "name": "TRAVAIL EXTRA LEGAL",
      "amountWithTaxes": 1220,
      "beneficiaryCode": "OGA-TRESOR"
    },
    {
      "code": "BCS",
      "name": "BENIN CONTROL SCANNING",
      "amountWithTaxes": 49159,
      "beneficiaryCode": "OGA-BCSCAN"
    },
    {
      "code": "PCS",
      "name": "PRELEVEMENT COMMUN SOLIDARITE",
      "amountWithTaxes": 91
      652,
      "beneficiaryCode": "OGA-UEMOA"
    },
    {
      "code": "PC",
      "name": "PRELEVEMENT COMMUNAUTAIRE",
      "amountWithTaxes": 57283,
      "beneficiaryCode": "OGA-CEDEAO"
    },
    {
      "code": "RS",
      "name": "REDEVANCE STATISTIQUE",
      "amountWithTaxes": 114565,
      "beneficiaryCode": "OGA-TRESOR"
    },
    {
      "code": "DD",
      "name": "DROIT DE DOUANE",
      "amountWithTaxes": 1145650,
      "beneficiaryCode": "OGA-TRESOR"
    },
    {
      "code": "RAU",
      "name": "REDEVANCE D'AMENAGEMENT URBAIN",
      "amountWithTaxes": 57283,
      "beneficiaryCode": "OGA-TRESOR-AU"
    },
    {
      "code": "TVA",
      "name": "TAXE SUR LA VALEUR AJOUTEE",
      "amountWithTaxes": 2326128,
      "beneficiaryCode": "OGA-TRESOR"
    },
    {
      "code": "AIB",
      "name": "ACCOMP. sr IMPOT ASSIS sr BENEFIC.",
      "amountWithTaxes": 387688,
      "beneficiaryCode": "OGA-TRESOR"
    }
  ],
  "invoiceBeneficiaries": [
    {
      "beneficiaryCode": "OGA-TRESOR",
      "amount": 4042534
    },
    {
      "beneficiaryCode": "OGA-BCSCAN",
      "amount": 49159
    },
    {
      "beneficiaryCode": "OGA-UEMOA",
      "amount": 91652
    },
    {
      "beneficiaryCode": "OGA-CEDEAO",
      "amount": 57283
    },
    {
      "beneficiaryCode": "OGA-TRESOR-AU",
      "amount": 57283
    }
  ],
  "additionalFields": [
    {
      "code": "documentDate",
      "value": "2021-10-15T00:00:00.000"
    },
    {
      "code": "customsType",
      "value": "SAD"
    },
    {
      "code": "customsDeclarantCode",
      "value": "201304"
    },
    {
      "code": "customsDeclarantName",
      "value": "ISAM GROUPE SARL\nC/345 MARROMILITAIRE\n03 BP 3592 JERICHO"
    },
    {
      "code": "customsRegistrationNumber",
      "value": "2021-BJC02-C-7996"
    },
    {
      "code": "customsCompanyCode",
      "value": "3202113292581"
    },
    {
      "code": "customsCompanyNameAddress",
      "value": "DEGOL BUSINESS"
    },
    {
      "code": "usn",
      "value": "USN-21014179"
    },
    {
      "code": "customsAssessmentNo",
      "value": "2021-BJC02-A-7996"
    },
    {
      "code": "customsDeclarationYear",
      "value": "2021"
    },
    {
      "code": "customsDeclarationReference",
      "value": "HIA-0613"
    },
    {
      "code": "customsModelOfDeclaration",
      "value": "IM 4"
    }
  ]
}
```

### Get Invoice Request Examples
This method allows the External System to check an Invoice ID and retrieve extra details. 

|Parameter Name|Data Type|Description|
|--|--|--|
|invoiceNumber|	String|	Invoice Number|
|userReference/ merchantReference	|String	|text used by the third-party to retrieve a list of invoices|

### Void Invoice Request Example
This endpoint allows the Merchant system to mark a finalized invoice as void. This cannot be undone.
|PATCH request|
|--|
|/v1/invoices/SAD2021-f3390/void|

## Webhook Notification
Webhooks are the notifications that are sent when the payment is executed in the system.

### Main field objects

|Parameter Name|Data Type|Description|
|--|--|--|
|id	|Long	|Webhook ID automatically generated by system at execution of webhook.|
|type	|String	|Invoice paid, invoice created, invoice viewed|
|id|	String	|Invoice ID|
|status	|String|	Invoice status|
|totalAmount	|bigDecimal	|Total amount to paid for the invoice.|
|documentNo	|String	|Merchant reference|
|paymentNo	|String	|Transaction reference|
|paymentDate	|ZonedDateTime|	Date of the payment execution|
|paymentMode|	String	|CASH, DIRECT_PAYMENT, MOBILE_MONEY|
|collector|	String	|Bank that processed the transaction|
|bankCode	|String|	Bank that processed the transaction|
|createdAt|	ZonedDateTime|	Webhook sending time|
|pendingWebhooks|	Integer	|The difference between Maximum retry attempts and the number of calls already executed.|

### Additional Field objects

|Parameter Name|Data Type|Mandatory/ optional|Description|
|--|--|--|--|
|code	|String	|optional	|Additional Field code|
|value|	String|	optional|	Additional Field value|

### Webhook Notification Body example
 ```
 "id":48379650,
"type":"invoice.paid",
"data":{
"id":"CNSS2022-3e4a4",
"status":"PAID",
"totalAmount":10,
"documentNo":"34417",
"paymentNo":"gucegpaac1d",
"paymentDate":"2021-11-26T09:02:38.232+01:00",
"paymentMode":"DIRECT_PAYMENT",
"collector":"GNUBA",
"bankCode":"GNUBA ",
"additionalFields":[
{
"code":"customsType",
"value":"CNSS"
}
]
},
"createdAt":"2021-11-26T09:05:12.075361216+01:00",
"pendingWebhooks":2
}

 ```

 ### Responses Status Codes (Code HTTP)
 The response body is optional and it will be logged in Database if sent.

 |Code|	Description|
 |--|--|
|200|	OK|
|204	|No content / Object deleted|
|400	|Bad Request / Returns JSON with the error message|
|401	|Unauthorized / Couldn't authenticate your request|
|404	|Not found /  No such object|
|500|	Internal Server Error|
|503|	Service Unavailable / The connection is being throttled or the service is down for maintenance|

#### Response Cases
**Case 1**:   If the received response is null, then we keep the corresponding webhook invocation as FAILED to be retried after a certain period (depending on the webhook retry configuration of each environment) and we set the error in Paylican Invoice to SYNCH_UNAVAILABLE.

**Case 2**:	If the HTTP response is returned with 2xx code then we mark the corresponding webhook invocation as SUCCESSFULL and we set its status to INVOCATION_SUCCESS. In case a content gets sent in the response body it gets saved in the Paylican Invoice database. 

**Case 3**:	If the HTTP response code IS NOT 2xx then we mark the corresponding webhook invocation as FAILED to be retried after a certain period (depending on the webhook retry configuration of each environment) and we set its status to INVOCATION_FAIL. In case a content gets sent in the response body it gets saved in the Paylican Invoice database. 

