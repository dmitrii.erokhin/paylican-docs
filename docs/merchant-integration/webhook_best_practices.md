---
sidebar_position: 5
---

# Webhook Integration: Verification and Best Practices

## Security

Merchants must keep their endpoints secure.

Paylican provides several ways for Merchants to verify events are coming from Paylican in a secure manner.

### Receive events with an HTTPS server

If Merchants use HTTPS URLs for their webhook endpoint, Paylican will validate that the connection to the Merchants servers are secure before sending webhook data.

For this to work, Merchants servers must be correctly configured to support HTTPS with a valid server certificate.

### Verify events are sent from Paylican

Verify webhook signatures to confirm that received events are sent from Paylican.

Additionally, Paylican sends webhook events from a set list of IP addresses. Only trust events coming from these IP addresses.

### Roll endpoint secrets

If Merchants need to change their secrets used for verifying events coming from Paylican, they can do so in the Webhooks section in their Dashboard. For each endpoint, they click Roll secret and they can choose to immediately expire the current secret, or delay for up to 24 hours to provide time to update the verification code on their server. During this time, multiple secrets are active for the endpoint and Paylican generates one signature for each secret until expiration.

## Paylican Webhook Signature verifier

Paylican signs webhook events with signatures. The key for signature header is "signature".

Verification of signatures allows you to verify that the notification was sent by Paylican and not by a third-party.

Signature header is of format:

t1=1595842271,v1=767feca076b3055c69ac2bf39568119ea2ac454dd756174e4cb76a10e4a63770,v2=d537154077442c6abe3a9e19598364dee48bb5c548100899230688f303f29c10


The timestamp in the header is prefixed with t1= which is used to prevent replay attacks.

The current timestamp in unix time is checked against t1='s value, the tolerated difference is set as 3 minutes but it can be changed.

Signatures are prefixed with v= followed by an integer. They are generated using HMAC with SHA-256.

At most, two webhook secrets can be active simultaneously which v1, v2 correspond to. If only one webhook secret is active, v2 will be omitted.

The class com.paylican.webhook.signature.verify.WebhookSignatureVerifier has one public method named verify which can assist you in verifying the signature.

The method accepts three parameters: * active secret which should be stored securely by the end user. * json which is the json for the event received, note that it must be minified, an example is included in resources folder in the project. * A map object which includes key value pairs for headers.

Method returns true for successful verification and false for unsuccessful.

Note that the headers should be split, i.e. they must first be split using , then each element must be split using = character as the separator before being added to the map.

