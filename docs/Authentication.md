---
sidebar_position: 2
title: Authentication
---
# Authentication to the Payment Platform

## Token Retrieval

When financial institutions or merchants need to make API calls to the Payment Platform, a robust and secure authentication process is in place through the Single Sign-On (SSO) application. This ensures that only authorized parties gain access to the Payment Platform's services. Below, we elucidate the sequence of steps involved:

1. **Operator Authentication**: To initiate the process, the designated Operator will utilize a single account specifically provided for accessing the SSO application. This account serves as the key to accessing the Payment Platform's resources.

2. **Login Request**: The Operator commences the authentication process by sending a login request to the SSO application. This request includes the Operator's credentials, affirming their identity.

3. **Token Generation**: Upon receiving the Operator's login request and verifying the provided credentials, the SSO application, acting as the gatekeeper, will generate a unique authentication token.

4. **Token Dispatch**: Subsequently, this newly created token is securely transmitted to the authorized financial institution or merchant, effectively granting them access to the services of the Payment Platform. The token acts as an electronic seal of approval, ensuring that only validated parties can interact with the Payment Platform's offerings.

## Code Sample for Token Retrieval

Below is a sample code snippet to assist you in acquiring the authentication token, which can be modified with your specific details:

```shell
curl -X POST 'https://sso-demo.paylican.com/auth/realms/PGW/protocol/openid-connect/token'/ 
-H "Content-Type: application/x-www-form-urlencoded"/
-d "username=<user>"/
-d 'password=<password>'/
-d 'grant_type=password'/
-d 'client_id=<client_id>'/
-d 'client_secret=<secret>'
```
Ensure the following modifications :
- Replace ```user``` with your username.
- Replace ```password``` with your password.
- Replace ```client_id``` with your client ID.
- Replace ```secret``` with your client secret.

## Example of Token Retrieval Response

```
HTTP/1.1 200 OK
Content-Type:application/json
Cache-Control: no-store
Pragma: no-cache
```

```json
{
    "access_token":"vugyTgy56vhg6WYgfcc3jvjhv8",
    "token_type":"bearer",
    "expires_in":3600,
    "refresh_token":"IlkjSDoi98JHVVGKLJH56ctuO",
    "scope":"create"
}
```
## Utilizing the Authentication Token
The Operator can now seamlessly consume the desired API by incorporating the authentication token, generated by the SSO, into the request headers.

```header: { 'Authorization':'Bearer'+ access_token }```


## Token Refresh

It's essential to note that access tokens have a limited lifespan. Once an access token expires, it can be regenerated by sending a POST request to the same URL, albeit this time utilizing the refresh token in place of the username and password.

```shell
-d 'grant_type=password'/
-d 'client_id=<client_id>'/
-d 'refresh_token=<token>'
```

The Payment Platform's authentication and token management system exemplify our commitment to security and seamless interaction. By following these steps, you ensure the confidentiality, integrity, and security of transactions within the Payment Platform, providing a trusted and secure environment for both clients and partners.