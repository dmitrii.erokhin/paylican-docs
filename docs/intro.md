---
sidebar_position: 1
title: Documentation Overview
---
# Paylican Payments and Invoicing Solutions

## Executive Summary
This document introduces the Paylican electronic invoicing/billing and payment platform, outlining our solution for seamless invoice integration, electronic payments, and invoice reconciliation.

Paylican FZ LLC is a company that specializes in digitizing invoices and streamlining the payment process. Established in 2014 in Switzerland and later relocated to Dubai in 2022, Paylican has honed its technical and functional expertise by serving numerous governments worldwide.

The Paylican electronic billing and payment platform accelerated the funds collection and distribution to beneficiaries. It achieves this through direct integrations with various financial institutions, including commercial banks, central banks, mobile money operators, and debit/credit card providers. Notably, countries like Benin, Guinea, Niger, and Panama have adopted Paylican for customs clearance payments, pre-clearance, and licenses, fully digitizing international trade payment processes.

## A Comprehensive Payment Solution
The Paylican payment solution is characterized by accessibility, efficiency, innovation, and simplicity. It aims to empowers everyone to conduct digital payments in a straightforward, secure, and real-time manner.

The Paylican payment platform comprises three primary systems:

### Paylican Invoice

This component of the platform enables users to settle their invoices electronically. 

### Paylican Gateway 
The aggregator consolidates various available payment methods, facilitating the electronic payment of invoices and notifying Merchant systems about the transaction's success. It offers a range of payment  methods that can be used to pay the bill in Paylican. Each of them is presented in a separate section:

- Direct Bank Transfer
- Cash payment
- Back-up solution
- Fetch payments
- Debit/Credit card payments
- Digital Wallets

### Paylican Audit 
The Paylican Audit platform provides a comprehensive audit of every transaction and offers automatic reconciliation. Our advanced dashboard and reporting tools enable participants to closely monitor their activity.

With the Paylican platform, we aim to simplify and enhance the payment process for all involved, ensuring it is secure, efficient, and easy to use.Payment solutions or payment gateway